package com.communication.model;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;


public class SMSMessage {

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @NotNull
    @Size(min=9, max=9)
    //@Pattern(regexp = "^6")
    @Pattern(regexp = "/^[6][0-9]{8}$/")
    private String phone;

    @NotEmpty
    @Size(max = 160)
    @Pattern(regexp = "^[a-zA-Z]+$")
    private String message;


}
