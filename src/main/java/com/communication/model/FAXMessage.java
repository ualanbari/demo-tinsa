package com.communication.model;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;


public class FAXMessage {

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @NotNull
    @Size(min=9, max=9)
    @Pattern( regexp = "^(91)", message = "Number must begin with 91.")
    private String phone;

    @NotEmpty
    @Size(max = 500)
    private String message;


}
