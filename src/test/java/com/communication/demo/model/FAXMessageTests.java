package com.communication.demo.model;

import com.communication.model.FAXMessage;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

@RunWith(SpringRunner.class)
public class FAXMessageTests {

    private Validator validator;

    @Before
    public void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    //TODO expresion first two characters is not working
//    @Test
//    public void numberBeginsWith91() {
//        FAXMessage faxMessage = new FAXMessage();
//        faxMessage.setMessage("This is a fake lorem ipsum");
//        faxMessage.setPhone("913245432");
//        Set<ConstraintViolation<FAXMessage>> violations = validator.validate(faxMessage);
//        Assert.assertTrue(violations.isEmpty());
//    }

    @Test
    public void numberBeginsWrongNumber() {
        FAXMessage faxMessage = new FAXMessage();
        faxMessage.setMessage("This is a fake lorem ipsum");
        faxMessage.setPhone("413453424");
        Set<ConstraintViolation<FAXMessage>> violations = validator.validate(faxMessage);
        Assert.assertFalse(violations.isEmpty());
    }
}
