package com.communication.demo.model;

import com.communication.model.SMSMessage;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

@RunWith(SpringRunner.class)
public class SMSMessageTests {

    private Validator validator;

    @Before
    public void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    //TODO expresion first two characters is not working
//    @Test
//    public void numberBeginsWithSix() {
//        SMSMessage smsMessage = new SMSMessage();
//        smsMessage.setMessage("Thisisafakeloremipsum");
//        smsMessage.setPhone("610555456");
//        Set<ConstraintViolation<SMSMessage>> violations = validator.validate(smsMessage);
//        Assert.assertTrue(violations.isEmpty());
//    }


    @Test
    public void numberBeginsWrongNumberAndWrongText() {
        SMSMessage smsMessage = new SMSMessage();
        smsMessage.setMessage("This is a fake lorem ipsum");
        smsMessage.setPhone("413453424");
        Set<ConstraintViolation<SMSMessage>> violations = validator.validate(smsMessage);
        Assert.assertFalse(violations.isEmpty());
    }
}
